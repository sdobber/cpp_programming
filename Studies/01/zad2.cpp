#include <iostream>
#include <iomanip>
#include <conio.h>
#include <math.h>
#include <cstdlib>

using namespace std;

class Rkwadratowe{
	public:
		float a, b, c, delta, x1, x2;
		char liczba_pierwiastkow;
		void czytaj_dane();
		void przetworz_dane();
		void wyswietl_wynik();
};

void Rkwadratowe::czytaj_dane(){
	cout << "Podaj wspolczynnik a rownania kwadratowego " << endl;
	cin >> a;
	if (a==0){
		cout << "Podales nieprawidlowa wartosc wspolczynika a" << endl;
		getch();
		exit (1);
	}
	else{
	cout << "Podaj wspolczynnik b rownania kwadratowego " << endl;
	cin >> b;
	cout << "Podaj wspolczynnik c rownania kwadratowego " << endl;
	cin >> c;
	}
}

void Rkwadratowe::przetworz_dane(){
	delta = b*b - 4*a*c;
	
	if (delta<0) liczba_pierwiastkow = 0;
	if (delta==0) liczba_pierwiastkow = 1;
	if (delta>0) liczba_pierwiastkow = 2;
	
	switch(liczba_pierwiastkow){
		case 1:
			x1 = (-b)/(2*a);
			break;
		case 2:
			{
				x1 = (-b-sqrt(delta))/(2*a);
				x2 = (-b+sqrt(delta))/(2*a);
			}
			break;
		default:
			break;
	}
	
}

void Rkwadratowe::wyswietl_wynik(){
	cout << fixed << setprecision(2);
	switch(liczba_pierwiastkow){
		case 1:
			cout << "Jedyny pierwiastek rownania jest rowny " << x1 << endl ;
			break;
		case 2:
			cout << "Pierwiastki rownania sa rowne " << x1 << " i " << x2 << endl;
			break;
		default:
			cout << "Nie istnieja pierwiastki rownania kwadratowego" <<endl;
			break;
	}
}

int main(){
	Rkwadratowe rownanie;
	rownanie.czytaj_dane();
	rownanie.przetworz_dane();
	rownanie.wyswietl_wynik();
	return 0;
}
