#include <iostream>
#include <iomanip>
#include <conio.h>
#include <stdlib.h>

using namespace std;

const int size = 10;
int tab[size][size];
int suma = 0;

class Matrix{
	public:
		void czytaj_dane(int tab[size][size], int size);
		void przetworz_dane(int tab[size][size], int size);
		void wyswietl_wynik(int tab[size][size], int size);
};

void Matrix::czytaj_dane(int tab[size][size], int size){
	int i,j;
	for(int i=0; i<size; i++){
		for(int j=0; j<size; j++){
			if(i==j){
				tab[i][j] = rand() % 10+1;
			}
			else {
				tab[i][j] = 0;
			}
		}
	}
}

void Matrix::przetworz_dane(int tab[size][size], int size){
	int i;
	for(i=0; i<size; i++){
		suma = suma+tab[i][i];
	}
}

void Matrix::wyswietl_wynik(int tab[size][size], int size){
	int i, j;
	for(i=0; i<size; i++){
		for(j=0; j<size; j++){
			cout << tab[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
	cout << "Suma cyfr na przekatnej jest rowna " << suma;
}

int main(){
	Matrix macierz1;
	int tab[size][size];
	macierz1.czytaj_dane(tab, size);
	macierz1.przetworz_dane(tab, size);
	macierz1.wyswietl_wynik(tab, size);
	return 0;
}
