#include <iostream>
#include <iomanip>
#include <conio.h>

using namespace std;

class Rectangle{
	public:
		float a, b, pole;
	void czytaj_dane();
	void przetworz_dane();
	void wyswietl_wynik();
};

void Rectangle::czytaj_dane(){
	cout << "Podaj dlugosc boku a" << endl;
	cin >> a;
	cout << "Podaj dlugosc boku b" << endl;
	cin >> b;
}

void Rectangle::przetworz_dane(){
	pole = a*b;
}

void Rectangle::wyswietl_wynik(){
	cout << fixed;
	cout << setprecision(2);
	cout << "Pole prostokata o bokach ";
	cout << a;
	cout << " i ";
	cout << b;
	cout << " jest rowne ";
	cout << pole << endl;
	getch();
}

int main(){
	Rectangle rect;
	rect.czytaj_dane();
	rect.przetworz_dane();
	rect.wyswietl_wynik();
	return 0;
}
