#include <iostream>
#include <fstream>
#include <conio.h>

using namespace std;
const int size = 10;
class matrix{
	public:
	void czytaj_dane(int tab[size][size], int size);
	void zapisz_dane_do_pliku(int tab[size][size], int size);
	void czytaj_dane_z_pliku(int tab1[size][size], int size);
};
void matrix::czytaj_dane(int tab[size][size], int size){
	int i, j;
	for(i=0; i<size; i++){
		for(j=0; j<size; j++){
			tab[i][j]=0;
		}
	}
	for(i=0; i<size; i++){
		tab[i][i]=1;
	}
}

void matrix::zapisz_dane_do_pliku(int tab[size][size], int size){
	int i, j;
	cout << "Zapisujemy dane do pliku" << endl;
	ofstream save("dane.txt");
	for(i=0; i<size; i++){
		for(j=0; j<size; j++){
			cout << tab[i][j] << " "; 
			save << tab[i][j] << " ";
		}
		cout << endl;
	}
	save.close();
}
void matrix::czytaj_dane_z_pliku(int tab1[size][size], int size){
	int i, j;
	cout << endl;
	cout << "Odczytujemy dane z pliku" << endl;
	ifstream loaded("dane.txt");
	if(loaded.good()){
		for(i=0; i<size; i++){
			for(j=0; j<size; j++){
				loaded >> tab1[i][j];
				cout << tab1[i][j] << " ";
			}
			cout << endl;
		}
		loaded.close();
	}
}

int main(){
	int tab[size][size];
	int tab1[size][size];
	matrix matrix1;
	matrix1.czytaj_dane(tab, size);
	matrix1.zapisz_dane_do_pliku(tab, size);
	matrix1.czytaj_dane_z_pliku(tab1, size);
	getch();
	return 0;
}
