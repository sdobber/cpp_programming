#include <iostream>
#include <fstream>
#include <conio.h>

const int n=10;
using namespace std;

class matrix{
	public:
		int a[n][n], b[n][n], c[n][n];
		void czytaj_dane();
		void przetworz_dane();
		void zapisz_dane_do_pliku();
		void czytaj_dane_z_pliku();
};

void matrix::czytaj_dane(){
	int i, j;
	cout << "Tworzymy tablice a" << endl;
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			a[i][j]=0;
		}
	}
	for(i=0; i<n; i++){
		a[1][i]=1;
	}
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
}
void matrix::przetworz_dane(){
	int i, j;
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			b[i][j] = a[j][i];
		}
	}
}
void matrix::zapisz_dane_do_pliku(){
	int i, j;
	cout << "Zapisujemy tablice b do pliku" << endl;
	ofstream save("dane.txt");
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			cout << b[i][j] << " ";
			save << b[i][j] << endl;
		}
		cout << endl;
	}
	save.close();
}
void matrix::czytaj_dane_z_pliku(){
	int i, j;
	cout << endl;
	cout << "Odczytujemy tablice b z pliku i przekazujemy do c" << endl;
	ifstream loaded("dane.txt");
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			loaded >> c[i][j];
			cout << c[i][j] << " ";
		}
		cout << endl;
	}
	loaded.close();
}

int main(){
	matrix matrix1;
	matrix1.czytaj_dane();
	matrix1.przetworz_dane();
	matrix1.zapisz_dane_do_pliku();
	matrix1.czytaj_dane_z_pliku();
	getch();
	return 0;
}
