#include <iostream>
#include <iomanip>
#include <conio.h>

using namespace std;
const int n = 6;
int tab[n];

class bsort{
	public:
		void czytaj_dane();
		void przetworz_dane();
		void wyswietl_wynik();
};

void bsort::czytaj_dane(){
	int i;
	cout << "Podaj 6 liczb" << endl;
	for(i=0; i<6; i++){
		cout << "Podaj liczbe" << endl;
		cin >> tab[i];
	}
	cout << "Liczby nieposortowane:" << endl;
	for(i=0; i<6; i++){
		cout << tab[i] << " ";
	}
	cout << endl;
}

void bsort::przetworz_dane(){
	int tmp, i, j;
	for(i=0; i<n; i++){
		for(j=1; j<n-i; j++){
			if(tab[j-1]>tab[j]){
				tmp=tab[j-1];
				tab[j-1]=tab[j];
				tab[j]=tmp;
			}
		}
	}
}

void bsort::wyswietl_wynik(){
	int i;
	cout << "Liczby posortowane:" << endl;
	for(i=0; i<6; i++){
		cout << tab[i] << " ";
	}
}

int main(){
	bsort srt;
	srt.czytaj_dane();
	srt.przetworz_dane();
	srt.wyswietl_wynik();
	getch();
	return 0;
}
