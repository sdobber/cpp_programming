#include <stdio.h>
#include <iostream>

using namespace std;

class pierwsza {
	public:
		int metoda1(int a, int b){
			return a+b;
		}
};
class druga {
	public:
		int metoda2(int a, int b){
			return a-b;
		}
};
class trzecia{
	public:
		int metoda3(int a, int b){
			return a*b;
		}
};
class czwarta{
	public:
		int metoda4(int a, int b){
			return a/b;
		}
};

class zbiorcza :pierwsza, druga, trzecia, czwarta{
	public:
		void metoda_all(){
			cout <<metoda1(5,4)<<endl;
			cout <<metoda2(4,1)<<endl;
			cout <<metoda3(15,3)<<endl;
			cout <<metoda4(15,3)<<endl;
		}
};

int main(){
	zbiorcza zbiorcza1;
	zbiorcza1.metoda_all();
	return 0;
}
