#include <stdio.h>
#include <iostream>

using namespace std;

class Figura{
	protected:
		double podstawa, wysokosc;
	public:
		double	przypisz(double a, double h){
			podstawa = a;
			wysokosc = h;
		}
};
class Kwadrat:public Figura{
	public:
	Kwadrat()
	{}
		double przypisz (double a, double h){
			return a*a;
		}
};

class Prostokat:public Figura{
	public:
		Prostokat()
		{}
		double przypisz(double a, double h){
			return a*h;
		}
};

class Trojkat: public Figura{
	public:
		Trojkat()
		{}
		double przypisz(double a, double h){
			return (a*h)/2;
		}
};

int main(){
	Kwadrat kwadrat1;
	Prostokat prostokat1;
	Trojkat trojkat1;
	
	cout << kwadrat1.przypisz(4.0,4.0);
	cout << prostokat1.przypisz(4.0,5.0);
	cout << trojkat1.przypisz(3.0, 4.0);
}
