#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <conio.h>

using namespace std;
const int n = 6;

class Lotto{
	public:
		void generate_data(int tab[n], int n);
		void display_data(int tab[n], int n);	
};

void Lotto::generate_data(int tab[n], int n){
	int i, a, j;
	bool flag;
	
	cout << "Program losuje 6 liczb z przedzialu od 1 do 49, liczby nie powtarzaja sie" << endl;
	for(i=0; i<n; i++){
		do{
			flag = true;
			a = rand() % 49+1;
			for(j=0; j<i; j++){
				if(a==tab[j]){
					flag=false;
					break;
				}
			}
		}while(!flag);
		tab[i]=a;
	}
}

void Lotto::display_data(int tab[n], int n){
	int i;
	for(i=0; i<n; i++){
		cout << tab[i] << " ";
	}
	cout << endl;
}

int main (){
	srand(time(NULL));
	int tab[n];
	Lotto lotto;
	lotto.generate_data(tab, n);
	lotto.display_data(tab, n);
	getch();
	return 0;
}
